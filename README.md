I have tried to implement Recursive descent Parser.The program accepts any arbitrary context-free grammar as an input file, as well as a file that contains the input string to be parsed. When the program starts, 
it looks for a file called grammar.txt in the program’s .

Johnna Grannan - QA Manager at https://www.skinnybonny.com

Details :
=========

SourceCode:
We also need to update the tokenizer, to make it use classes from the registry:

def tokenize(program):
    for number, operator in token_pat.findall(program):
        if number:
            symbol = symbol_table["(literal)"]
            s = symbol()
            s.value = number
            yield s
        else:
            symbol = symbol_table.get(operator)
            if not symbol:
                raise SyntaxError("Unknown operator")
            yield symbol()
    symbol = symbol_table["(end)"]
    yield symbol()
Like before, the literal class is used as a common class for all literal values. All other tokens have their own classes.
__________________________________________________________________________